/*
Posizionarsi nella cartella src e compilare come segue:

	javac -d ../classes editdistanceusage/*.java

Posizionarsi nella cartella classes ed eseguire il programma

		java editdistanceexample/EditDistanceUsageExample ../correctme.txt ../dictionary.txt

*/

package editdistanceexample;
import editdistance.EditDistance;
import java.util.Scanner;
import java.io.*;
import java.util.*;

/**
 * Usage:
 *    EditDistanceUsageExample <filename>
 *
 * @author Vincenzo Carotenuto
 */

public class EditDistanceUsageExample{
	public static void main(String[] args){
		try{
			if(args.length < 1) {
				throw new Exception("Wrong number of arguments provided to the program.");
			}
			readWordFile(args[0], args[1]);
		}
		catch( Exception e ) {
			System.out.println(e.getMessage());
		}
	}
/**
  * The method reads each word within <filename1> e <filename2> 
  * @param filename1, filename2
  * 
  */
	
	
	public static void readWordFile(String filename1, String filename2) throws IOException{
		Scanner input = new Scanner(new File(filename1));
		ArrayList<String> correctmeList = new ArrayList<>();
		Scanner dictionary = new Scanner(new File(filename2));
		ArrayList<String> dictionaryList = new ArrayList<>();
		String word;
		while (input.hasNext()) {
			word  = input.next().toLowerCase().trim();
			word = word.replaceAll(",","").replaceAll(":","").replaceAll("\\.","");
			correctmeList.add(word);
		}
		while(dictionary.hasNext()){
			dictionaryList.add(dictionary.nextLine().trim());
		}
		String[] correctmeArray = correctmeList.toArray(new String[correctmeList.size()]);
		String[] dictionaryArray = dictionaryList.toArray(new String[dictionaryList.size()]);
		
		application(correctmeArray, dictionaryArray);
	}
	
	/**
	* The method calculate for each couple of word the editDistanceDyn
	*
	*/
	public static void application(String[] s1, String[] s2){
		for(int i=0;i<s1.length-1;i++){
			int min = 1000;
			ArrayList<String> v = new ArrayList<>();
			for (int j=0; j<s2.length-1; j++){
				int tmp = EditDistance.editDistanceDyn(s1[i],s2[j]);
				if(tmp<=min){
					if(tmp<min){
						v = new ArrayList<String>();
					}
					min = tmp;
					v.add(s2[j]);
					if(min==0) break;
					
				}	
			}
			
			printListWord(s1[i], v);
		}
	}
	
	
	private static void printListWord (String word, ArrayList<String> listword) {
		System.out.printf("\n%40s\t%40s\n\n", "[Word]", "[List]");
		System.out.printf("%40s\t%40s\n", word, listword.get(0));
		for (int i=1;i<listword.size();i++)
			System.out.printf("%40s\t%40s\n", "", listword.get(i));
	}
}
