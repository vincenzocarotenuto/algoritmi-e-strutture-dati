package editdistance;

import java.util.Arrays;

/**
  * @author Vincenzo Carotenuto
  * @param x,y: two fields of type string for which we want to calculate edit distance
  */

public class EditDistance{
  /**
    * recursive version of the edit_distance algorithm
    * the algorithm calculates the distance between two words
    * @return: an integer
    */
	public static int editDistanceRec(String x, String y) {
		if (x.isEmpty()) {
			return y.length();
		}

		if (y.isEmpty()) {
			return x.length();
		}

		int substitution = editDistanceRec(x.substring(1), y.substring(1))
		+ costOfSubstitution(x.charAt(0), y.charAt(0));
		int insertion = editDistanceRec(x, y.substring(1)) + 1;
		int deletion = editDistanceRec(x.substring(1), y) + 1;

		return Math.min(Math.min(substitution, insertion), deletion);
	}
 /**
   * Dynamic version of the edit_distance algorithm.
		dp[i][j] = min( dp[i-1][j]+1,
						dp[i][j-1]+1,
						dp[i-1][j-1]+ (	0 se dp[i]==dp[j]
										2 se dp[i]!=dp[j])
					)
   * @return: an integer
   */
   
	public static int editDistanceDyn(String x, String y) {
		int[][] dp = new int[x.length() + 1][y.length() + 1];
		for (int i = 0; i <= x.length(); i++) {
			for (int j = 0; j <= y.length(); j++) {
				if (i == 0) {
					dp[i][j] = j;
				}
				else if (j == 0) {
					dp[i][j] = i;
				}
				else {
					dp[i][j] = min(dp[i - 1][j - 1]
					+ costOfSubstitution(x.charAt(i - 1), y.charAt(j - 1)),
					dp[i - 1][j] + 1,
					dp[i][j - 1] + 1);
				}
			}
		}
		return dp[x.length()][y.length()];
	}

	private static int costOfSubstitution(char a, char b) {
		return a == b ? 0 : 2;
	}
 /**
   * Calculate the minimum within the array.
   * @return: value the minimum (an integer)
   */
	private static int min(int a, int b, int c) {
		int min=a;
		
		if(b<min) min=b;
		if(c<min) min=c;
		
		return min;
	}	

}
