/*
Posizionarsi nella cartella src e compilare come segue:
cd C:\Users\Vincenzo\Documents\Università\Algoritmi\Progetto2_Algoritmi\src

javac -d ../classes editdistance/EditDistance.java

javac -d ../classes -cp .;C:\ProgramData\JUnit\junit-4.12.jar;C:\ProgramData\JUnit\hamcrest-core-1.3.jar editdistance/*.java

Posizionarsi nella cartella classes ed eseguire come segue:
cd C:\Users\Vincenzo\Documents\Università\Algoritmi\Progetto2_Algoritmi\classes

java -cp .;C:\ProgramData\JUnit\junit-4.12.jar;C:\ProgramData\JUnit\hamcrest-core-1.3.jar editdistance.EditDistanceJava_TestsRunner

oppure

java -cp .;C:\ProgramData\JUnit\junit-4.12.jar;C:\ProgramData\JUnit\hamcrest-core-1.3.jar org.junit.runner.JUnitCore editdistance.EditDistanceTests
*/
package editdistance;

import java.util.Comparator;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
/**
  *  @author Vincenzo Carotenuto
  */
public class EditDistanceTests{

	private String first, second;
	/*
	* the test checks whether the recursive method returns
	* the expected value when empty strings are passed
	*/
	@Test
	public void testIsEmptyRec(){
		first = "";
		second = "";
		assertEquals(0, EditDistance.editDistanceRec(first,second));
	}

	/*
	* the test checks whether the dynamic method returns
	* the expected value when empty strings are passed
	*/
	@Test
	public void testIsEmptyDyn(){
		first = "";
		second = "";
		assertEquals(0, EditDistance.editDistanceDyn(first,second));
	}

	/*
	* the test checks whether the recursive method returns
	* the expected value when passing strings with causal values
	*/
	@Test
	public void twoWordRec(){
		first = "Ciao";
		second = "Cioa";
		assertEquals(2, EditDistance.editDistanceRec(first,second));
	}

	/*
	* the test checks whether the dynamic method returns
	* the expected value when passing strings with causal values
	*/
	@Test
	public void twoWordDyn(){
		first = "Ciao";
		second = "Cioa";
		assertEquals(2, EditDistance.editDistanceDyn(first,second));
	}

}
