package graphusage;
/*
Posizionarsi nella cartella src e compilare come segue:

javac -d ../classes graphusage/*.java

Posizionarsi nella cartella classes ed eseguire il programma

		java graphusage/LabelledGraphUsage "../italian_dist_graph.csv"
*/
/**
 *
 * @author Vincenzo Carotenuto
 */
 
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import graph.LabelledGraph;
import graph.Vertex;
import graph.Edge;
import priorityqueue.*;

public class LabelledGraphUsage {

 /**
  * Load the data into the Graph.
  */ 
	private static Vertex<String> loadGraph(String filepath, LabelledGraph<String,Float> graph) throws IOException, Exception{
		if(graph != null){
			Vertex<String> tmp = null;
			System.out.println("\nYou are loading the data from the file\n");
			Path inputFilePath = Paths.get(filepath);
			try(BufferedReader fileInputReader = Files.newBufferedReader(inputFilePath)){
				String line = null;
				while((line = fileInputReader.readLine()) != null){      
					String[] firstSplit = line.split(",");
					Vertex<String> vx1 = new Vertex<String>(firstSplit[0]);
					if(tmp == null)
						tmp = vx1;
					Vertex<String> vx2 = new Vertex<String>(firstSplit[1]);
					Edge<String,Float> eg = new Edge<String,Float>(vx1,vx2,Float.parseFloat(firstSplit[2]));      
					graph.insertVertex(vx1);
					graph.insertVertex(vx2);
					graph.insertEdge(eg);
				}
			} 
			System.out.println("\nData loaded\n");
			return tmp;
		}else
			throw new Exception("You cannot insert vertex if Graph is null!");
	}
  
	public static void main(String[] args)throws Exception, Exception, IOException{
		LabelledGraph<String,Float> graph = new LabelledGraph<String,Float>(false);
		Vertex<String> root = loadGraph(args[0],graph);
		graph = graph.prim(root,Float.MIN_VALUE,Float.MAX_VALUE);
		System.out.println("Nodes:"+graph.getVertexNumber());
		System.out.println("Edges:"+graph.getEdgeNumber());
		graph.printGraph();
		System.out.format("Weight:%7f\n", graph.getWeight());
	}
  
}
