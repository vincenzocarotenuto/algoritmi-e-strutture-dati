package graph;

import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author Vincenzo Carotenuto
 * @param <T>: type of the Vertex elements
 */
 
public class Vertex<T>{
	private T value;
	private int color;
	
	public Vertex(T value){
		this.value = value;
		this.color = 0;  
	}
	
	public T getValue(){
		return value;
	}
	
	public void setColor(int color){
		this.color = color;
	}
	
	public int getColor(){
		return color;
	}
	
	@Override
	public boolean equals(Object obj){
		if(obj == null)
			return true;
		if( obj instanceof Vertex<?>){ 
			if(((Vertex<?>)obj).getValue().equals(value))
				return true;
		}
		return false;
	}
	
	@Override 
	public int hashCode(){
		return value.hashCode();
	}
}