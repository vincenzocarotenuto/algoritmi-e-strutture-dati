package graph;

/**
 *
 * @author Vincenzo Carotenuto
 * @param <T>: type of the Vertex elements
 * @param <E>: type of the Edge elements
 */

public class Edge<T,E> {
	private Vertex<T> vx1;
	private Vertex<T> vx2; 
	private E label;
  
	public Edge(Vertex<T> v1,Vertex<T> v2,E value){
		this.vx1 = v1;
		this.vx2 = v2;
		label = value;
	}
  
	public Edge(Vertex<T> v1,Vertex<T> v2){
		this.vx1 = v1;
		this.vx2 = v2;
		this.label = null;
	}

	public Vertex<T> getVertex1(){
		return vx1;
	}
  
	public Vertex<T> getVertex2(){
		return vx2;
	}
  
	public E getLabel(){
		return label;}
  
	public Edge<T,E> transposedEdge(){
		Edge<T,E> newEdge;
		if(label != null)
			newEdge = new Edge<T,E>(vx2,vx1,label);
		else
			newEdge = new Edge<T,E>(vx2,vx1);
		return newEdge;
	}
  
	@Override
	public boolean equals(Object obj){	
		if(obj instanceof Edge<?,?>){
			if(label == null){
				return vx1.equals(((Edge<?,?>)obj).getVertex1()) && vx2.equals(((Edge<?,?>)obj).getVertex2());
			}else
				return vx1.equals(((Edge<?,?>)obj).getVertex1()) && vx2.equals(((Edge<?,?>)obj).getVertex2()) 
				&& label.equals(((Edge<?,?>)obj).getLabel());
		}
		return false;
	}
	
	@Override
	public int hashCode(){
		if(label == null)
			return vx1.hashCode() +vx2.hashCode();
		else
			return vx1.hashCode() + vx2.hashCode() + label.hashCode();
	}  
}
