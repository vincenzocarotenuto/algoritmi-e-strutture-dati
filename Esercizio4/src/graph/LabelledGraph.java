package graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Iterator;
import queue.*;
import tree.*;
import priorityqueue.*;

/**
  * @author Vincenzo Carotenuto
  * @param <T>: type of vertex value.
  * @param <E>: type of edge label.
  */

public class LabelledGraph<T,E extends Comparable<E>> {
	private HashMap<Integer,HashSet<Edge<T,E>>> adjList;
	private boolean direct;
	private Scanner sc = new Scanner(System.in);
	private HashSet<Vertex<T>> vertexList;
	
	
	
	public LabelledGraph(boolean direct){
		this.adjList = new HashMap<Integer,HashSet<Edge<T,E>>>();
		this.direct = direct;
		this.vertexList = new HashSet<Vertex<T>>();
	}
	
	/**
	  *@return true if the graph is empty.
      */  
	public boolean isEmpty(){
		return adjList.isEmpty();
	}
	
	/**
	  * @return The number of key-value pair mapping, HashMap contains. 
	  */
	public int size(){
		return adjList.size();
	}
	
	/** 
	  * @return true if the Vertex is in the Edge Set
	  */
	public boolean contains(Vertex<T> v){
		return vertexList.contains(v);
	}
	
	/**
	  * @return the number of Vertex in the Graph
	  */
	public int getVertexNumber(){
		return vertexList.size();
	}	
	
	
	/**
	  * @return the number of the Edge in the graph
	  */
	public int getEdgeNumber(){
		int tmp = 0;
		for(Vertex<T> v: vertexList){
			tmp += adjList.get(v.hashCode()).size();
		}
		if(direct)
			return tmp;
		else
			return tmp/2;
	}
	
	/**
	  * @return the weight of the graph (in double): If the graph is not direct
      * the weight is divided by 2 because it is calculated 2 times.
      */ 
	public double getWeight() throws Exception{
		double weight = 0;
		for(Vertex<T> v: vertexList)
			for(Edge<T,E> eg: adjList.get(v.hashCode()))
				if(eg.getLabel()!=null && eg.getLabel() instanceof Number){
					Number we = (Number) eg.getLabel();
					weight =  weight +  we.doubleValue();
				}
				else{
					throw new Exception("You can't return the weight of the graph it it's null or if it's not a number!");
				}
		if(!direct)
			weight = weight/2;
		return weight;
	}
	
	/**
      * @param vertex: vertex to be added in the graph.
      * @return true if vertex does not exist and add it,
      * if not, return false.
      */
	public boolean insertVertex(Vertex<T> vx) throws Exception{
		if(vx == null)
			throw new Exception("You can't insert a Vertex that is null!");
		if(!adjList.containsKey(vx.hashCode())){
			HashSet<Edge<T,E>> adj = new HashSet<Edge<T,E>>();
			vertexList.add(vx);
			adjList.put(vx.hashCode(),adj);
			return true;
		}
		return false;
	}
	
	/**
	  * @param edge: edge to be added in the graph.
      * @return true if edge does not exist and add it,
      * if not, you can choose to end the execution or return false.
      */
	public boolean insertEdge(Edge<T,E> eg) throws Exception{
		if(eg == null)
			throw new Exception("You can't insert an Edge that is null!");
		if(!adjList.containsKey(eg.getVertex1().hashCode()))
			throw new Exception("You can't insert an edge if the first vertex is not inserted!");
		else if(!adjList.containsKey(eg.getVertex2().hashCode()))
			throw new Exception("You can't insert an edge if the second vertex is not inserted!");		
		else{
			if(adjList.get(eg.getVertex1().hashCode()).contains(eg)){
				return false;
			}
			if(!direct){ 
				if(eg.getVertex1().getValue() == eg.getVertex2().getValue())
					throw new Exception("You can't insert an edge in the same vertex if the graph isn't direct");
				adjList.get(eg.getVertex2().hashCode()).add(eg.transposedEdge());
			}
			adjList.get(eg.getVertex1().hashCode()).add(eg);
		}
		return true;
	}
	
	/**
      * @param vx1: vertex to be deleted.
      * the method remove vx1 from the adiacency list and remove
      * all the edge that contained the vertex vx1. 
      * @return true if the vertex is deleted or false if is not.
      */
	public boolean deleteVertex(Vertex<T> vx1){
		if(vx1 != null){
			if(adjList.containsKey(vx1.hashCode())){
				if(adjList.get(vx1.hashCode()).isEmpty()){
					adjList.remove(vx1.hashCode());
					vertexList.remove(vx1);
					return true;
				}else{
				if(!direct){
					for(Edge<T,E> eg: adjList.get(vx1.hashCode())){
						Edge<T,E> traspEdge = eg.transposedEdge();
						adjList.get(traspEdge.getVertex1().hashCode()).remove(traspEdge);
					}
				}
				adjList.get(vx1.hashCode()).clear();
				adjList.remove(vx1.hashCode());
				vertexList.remove(vx1);
				return true;
				}
			}
		}
		return false;
	}
	
	/**
      * @param eg: Edge to be deleted. 
      */
	public boolean deleteEdge(Edge<T,E> eg){
		boolean delete1,delete2;
		if(eg != null){
			delete1 = adjList.get(eg.getVertex1().hashCode()).remove(eg);
			if(!direct){
				Edge<T,E> traspEdge = eg.transposedEdge();
				delete2 = adjList.get(traspEdge.getVertex1().hashCode()).remove(traspEdge);
				return delete2 && delete1;
			}
			return delete1;
		}
		return false;
	}
	
	  /**
        * @param vx1: vertex to be checked
        * @param vx2: second vertex of the edge
        * @return true if the vertex contains the edge.
        */
	public boolean contains(Vertex<T> vx1,Vertex<T> vx2){
		for(Edge<T,E> eg: adjList.get(vx1.hashCode()))
			if(eg.getVertex2() == vx2)
				return true;
		return false;
	}
	
	/**
      * Reset the color of all vertex to 0.
      */
	public void resetColor(){
		for(Vertex<T> v: vertexList)
			v.setColor(0);
	}
	
	/**
	  *	Print all the Vertex with his Edge.
      */
	public void printGraph(){
		Iterator<Edge<T,E>> iterator_edge;
		Iterator<Vertex<T>> iterator_vertex = vertexList.iterator();
		Vertex<T> tmp_vertex;
		Edge<T,E> tmp_edge;
		while(iterator_vertex.hasNext()){
			tmp_vertex = iterator_vertex.next();
			System.out.print(" V : "+tmp_vertex.getValue());
			iterator_edge =  adjList.get(tmp_vertex.hashCode()).iterator();
			System.out.print(" E : ");
			while(iterator_edge.hasNext()){
				tmp_edge = iterator_edge.next();
				System.out.print(tmp_edge.getVertex2().getValue()+" ");
				if(tmp_edge.getLabel() != null){
					System.out.print("Label: "+tmp_edge.getLabel()+", ");
				}
			}
			System.out.println("\n");
		}
	}
	
	/**
      * @param v: starting point for the visit.
      * @return the tree created during the BFS  visit.
      * we use an iterator to search into the Hashset of every single vertex.
	  * |E|+|V|
      */
	public KTree<T> breadthFirstSearch(Vertex<T> v) throws Exception{
		Vertex<T> v2,current;
		KTree<T> ktree = new KTree<T>(v.getValue());
		Queue<Vertex<T>> queue = new Queue<Vertex<T>>();
		Iterator<Edge<T,E>> iterator;
		v.setColor(1);
		queue.enqueue(v);
		while(!queue.empty()){
			current = queue.first();
			iterator = adjList.get(current.hashCode()).iterator();
			while(iterator.hasNext()){
				v2 =  iterator.next().getVertex2();
				if(v2.getColor() == 0){
					v2.setColor(1);
					ktree.insertChild(current.getValue(),v2.getValue());
					queue.enqueue(v2);
				}
			} 	
			current.setColor(2);
			queue.dequeue();
		}
		resetColor();
		return ktree;
	}
	
	/**
      * @param v: starting point for the visit.
      * @return the tree created during the DFS  visit.
      * we use a wrapper method to create the tree.
      */
	
	public KTree<T> deepFirstSearch(Vertex<T> v) throws Exception{
		KTree<T> ktree = new KTree<T>(v.getValue());
		deepFirstSearchWrapper(v,ktree);
		resetColor();
		return ktree;
	}
	
	/**
      * @param v: current vertex of the visit.
      * we use an iterator to search into the Hashset of every single vertex.
      */

	private void deepFirstSearchWrapper(Vertex<T> v,KTree<T> ktree)throws Exception{
		Iterator<Edge<T,E>> iterator;
		Vertex<T> v2;
		v.setColor(1);
		iterator = adjList.get(v.hashCode()).iterator();
		while(iterator.hasNext()){
			v2 =  iterator.next().getVertex2();
			if(v2.getColor() == 0){
				v2.setColor(1);
				ktree.insertChild(v.getValue(),v2.getValue());
				deepFirstSearchWrapper(v2,ktree);
			}
		}
	}
	
	private void makePriorityQueue(PriorityQueue<Vertex<T>,E> heap,HashSet<Vertex<T>> vertexList, E maxValue)throws Exception{
		for(Vertex<T> v: vertexList){
			if(v != heap.getElem(0))
				heap.insert(v,maxValue);
		}
	}
	
	/**
      * @param root: the vertex of the graph where the algorithm will start.
      * @param comparator: the comparator used to determine the order reletionship between the elements of the graph.
      * @param minValue: the root's minimum weight, used to start the Prim algorithm.
      * @param maxValue: the maximum weight a edge can get, used to choose the next element to be added to the MST.
      * @return the graph's minimum spanning tree.
      */

	public LabelledGraph<T,E>prim(Vertex<T> root,E minValue, E maxValue) throws Exception{
		if(direct)
			throw new Exception("prim:The graph must not be direct");
		if(root == null)
			throw new Exception("Prim:root can't be NULL");
		if(!vertexList.contains(root))
			throw new Exception("Prim:root must be part of the graph");
		if(minValue == null)
			throw new Exception("Prim:minValue can't be NULL");
		if(maxValue == null)
			throw new Exception("Prim:maxValue can't be NULL");
		if(minValue.compareTo(maxValue) >= 0)
			throw new Exception("Prim:minValue must be smaller than maxValue");
		return primWorker(root,minValue,maxValue);
	}

	private void valueChecker(E value, E minValue, E maxValue)throws Exception{
		if(value.compareTo(minValue) < 0)
			throw new Exception("prim:minValue must be smaller than any value");
		if(value.compareTo(maxValue) > 0)
			throw new Exception("prim:maxValue must be greater than any value");
	}

	private LabelledGraph<T,E>primWorker(Vertex<T> root,E minValue,E maxValue) throws Exception{
		LabelledGraph<T,E> mst= new LabelledGraph<T,E>(false);
		PriorityQueue<Vertex<T>,E> heap = new PriorityQueue<Vertex<T>,E>();
		HashMap<Integer,Vertex<T>> father = new HashMap<Integer,Vertex<T>>();
		Vertex<T> tmp;
		PriorityIndex<E> priority = new PriorityIndex<E>();
		heap.insert(root,minValue);
		makePriorityQueue(heap,vertexList,maxValue);
		while(!heap.isEmpty()){
			tmp = heap.extractMinimum(priority);
			mst.insertVertex(tmp);
			if(father.containsKey(tmp.hashCode()))
				mst.insertEdge(new Edge<T,E>(tmp,father.get(tmp.hashCode()),priority.getPriority()));
			HashSet<Edge<T,E>> adj = adjList.get(tmp.hashCode()); 
			for(Edge<T,E> edge: adj){
				Vertex<T> target = edge.getVertex2();
				valueChecker(edge.getLabel(),minValue,maxValue);
				if(!mst.contains(target) && edge.getLabel().compareTo(heap.getPriority(target)) < 0){
					heap.updatePriority(target,edge.getLabel());
					father.put(target.hashCode(),tmp);
				}
			}
		} 
		return mst;
	} 
	
}
