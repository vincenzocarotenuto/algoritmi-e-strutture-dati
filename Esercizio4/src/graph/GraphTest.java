package graph;


/*
Posizionarsi nella cartella src e compilare come segue:

javac -d ../classes graph/Edge.java
javac -d ../classes graph/Vertex.java
javac -d ../classes priorityqueue/*.java
javac -d ../classes tree/*.java
javac -d ../classes queue/*.java
javac -d ../classes graph/LabelledGraph.java

javac -d ../classes -cp .;C:\ProgramData\JUnit\junit-4.12.jar;C:\ProgramData\JUnit\hamcrest-core-1.3.jar graph/*.java


Posizionarsi nella cartella classes ed eseguire come segue:

java -cp .;C:\ProgramData\JUnit\junit-4.12.jar;C:\ProgramData\JUnit\hamcrest-core-1.3.jar graph.Graph_TestRunner

oppure

java -cp .;C:\ProgramData\JUnit\junit-4.12.jar;C:\ProgramData\JUnit\hamcrest-core-1.3.jar org.junit.runner.JUnitCore graph.GraphTest
*/


import java.util.Comparator;
import priorityqueue.PriorityQueue;
import priorityqueue.PriorityIndex;
import tree.KTree;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Vincenzo Carotenuto
 */

public class GraphTest {
	private LabelledGraph<Integer,Integer> graph,graph1;
	private Vertex<Integer> vx1,vx2,vx3,vx4;
	private Edge<Integer,Integer> ed1,ed2,ed3,ed4,ed5,ed6;

	@Before
	public void createLabelledGraph(){
		vx1 = new Vertex<Integer>(5);
		vx2 = new Vertex<Integer>(7);
		vx3 = new Vertex<Integer>(2);
		vx4 = new Vertex<Integer>(9);
		ed1 = new Edge<Integer,Integer>(vx1,vx2,14);
		ed2 = new Edge<Integer,Integer>(vx1,vx3,10);
		ed3 = new Edge<Integer,Integer>(vx1,vx4,10);
		ed4 = new Edge<Integer,Integer>(vx2,vx3,10);
		ed5 = new Edge<Integer,Integer>(vx2,vx4,10);
		ed6 = new Edge<Integer,Integer>(vx3,vx4,10);
		graph = new LabelledGraph<>(true);
		graph1 = new LabelledGraph<>(false);
	}
	
	@Test
	public void testIsEmpty_zeroEl(){
		assertTrue(graph.isEmpty());
	}

	@Test
	public void testIsEmpty_oneVertex() throws Exception{
		graph.insertVertex(vx1);
		assertFalse(graph.isEmpty());
	}

	@Test
	public void testsize_oneVertex() throws Exception{
		graph.insertVertex(vx1);
		assertTrue(graph.size() == 1);
	}

	@Test
	public void testSize_containsVertex() throws Exception{
		graph.insertVertex(vx1);
		graph.insertVertex(vx2);
		assertTrue(graph.contains(vx1));
		assertTrue(graph.contains(vx2));
	}

	@Test
	public void testTrue_containsEdge() throws Exception{
		graph.insertVertex(vx1);
		graph.insertVertex(vx2);
		graph.insertEdge(ed1);
		assertTrue(graph.contains(vx1,vx2));
	}	

	@Test
	public void testFalse_containsEdge() throws Exception{
		graph.insertVertex(vx1);
		graph.insertVertex(vx2);
		graph.insertEdge(ed1);
		assertFalse(graph.contains(vx2,vx1));
	}

	@Test
	public void testTransposedEdge() throws Exception{
		Edge<Integer,Integer> tmp;
		graph.insertVertex(vx1);
		graph.insertVertex(vx2);
		graph.insertEdge(ed1);
		tmp = ed1.transposedEdge();
		assertTrue(tmp.getVertex1().equals(vx2) && tmp.getVertex2().equals(vx1));
	}
	
	@Test
	public void testDeleteVertex() throws Exception{
		graph.insertVertex(vx1);
		graph.insertVertex(vx2);
		graph.insertEdge(ed1);
		graph.deleteVertex(vx1);
		assertTrue(!graph.contains(vx1));
	}

	@Test
	public void testDeleteEdge() throws Exception{
		graph.insertVertex(vx1);
		graph.insertVertex(vx2);
		graph.insertEdge(ed1);
		graph.deleteEdge(ed1);
		assertFalse(graph.contains(vx1,vx2));
	}

	@Test
	public void testDeleteVertex_NotDirectGraph() throws Exception{
		graph1.insertVertex(vx1);
		graph1.insertVertex(vx2);
		graph1.insertEdge(ed1);
		graph1.deleteVertex(vx1);
		assertTrue(!graph1.contains(vx2,vx1) && !graph1.contains(vx1));
	}
	
	@Test
	public void testGetWeight() throws Exception{
		graph.insertVertex(vx1);
		graph.insertVertex(vx2);
		graph.insertEdge(ed1);
		assertTrue(graph.getWeight() == 14);	
	}

	@Test
	public void testGetWeight_notDirect() throws Exception{
		graph1.insertVertex(vx1);
		graph1.insertVertex(vx2);
		graph1.insertEdge(ed1);
		assertTrue(graph1.getWeight() == 14);	
	}
	
	@Test
	public void testPrimNoEdge() throws Exception{
		graph1.insertVertex(vx1);
		graph1.insertVertex(vx2);
		graph1.insertVertex(vx3);
		LabelledGraph<Integer,Integer> mst = graph1.prim(vx1,Integer.MIN_VALUE,Integer.MAX_VALUE);
		assertTrue(mst.getVertexNumber() == 3 && mst.getEdgeNumber() == 0 && mst.getWeight() == 0);
	}

	@Test
	public void testPrimOneVertex() throws Exception{
		graph1.insertVertex(vx1);
		LabelledGraph<Integer,Integer> mst = graph1.prim(vx1,Integer.MIN_VALUE,Integer.MAX_VALUE);
		assertTrue(mst.getVertexNumber() == 1 && mst.getEdgeNumber() == 0 && mst.getWeight() == 0);
	}

	@Test
	public void testPrimHeavilyConnected() throws Exception{
		graph1.insertVertex(vx1);
		graph1.insertVertex(vx2);
		graph1.insertVertex(vx3);
		graph1.insertVertex(vx4);
		graph1.insertEdge(ed1);
		graph1.insertEdge(ed2);
		graph1.insertEdge(ed3);
		graph1.insertEdge(ed4);
		graph1.insertEdge(ed5);
		graph1.insertEdge(ed6);
		LabelledGraph<Integer,Integer> mst = graph1.prim(vx1,Integer.MIN_VALUE,Integer.MAX_VALUE);
		assertTrue(mst.getVertexNumber() == 4 && mst.getEdgeNumber() == 3 && mst.getWeight() == 30);
	}
}	
