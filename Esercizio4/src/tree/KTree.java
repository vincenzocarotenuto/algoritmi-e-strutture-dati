package tree;

import java.util.HashSet;

/**
 *
 * @author Vincenzo Carotenuto
 * @param <E>: type of the KTree elements
 */


public class KTree<E>{
	private Node<E> root;

	public KTree(E elem){
		Node<E> newElem = new Node<E>(elem,null,null);
		root = newElem;
	}	

    /**
      * @param father: value of  children's father.
      * @param child: value of the new children to be inserted.
      * @return true if the child is inserted under the father, otherwise 
      * an exception is thrown.
      */
	public boolean insertChild(E father,E child) throws Exception{
		Node<E> tmp = root;
		if(!insertChildWrapper(tmp,child,father)){
			throw new Exception("You can't insert a child if the father doesn't exist!");
		}
		return true;
	}

	/**
      * @param father: value of children's father.
      * @param child: value of the new children to be inserted.
      * @param tmp: Temporary node to move on the tree.
      * if child is contained into the tree an exception is thrown.
      * @return true if the child is inserted into tree under 
      * the father or false otherwise. 
      */
	private boolean insertChildWrapper(Node<E> tmp,E child,E father) throws Exception{
		if(containsWrapper(tmp,child))
			throw new Exception("You can't insert the same element in tree");
		if(tmp != null){
			if(tmp.getElem() == father){
				if(tmp.getChild() == null){
					Node<E> node = new Node<E>(child,null,null);
					tmp.setChild(node); 
				}else{
					tmp = tmp.getChild();
					while(tmp.getSibling() != null)
						tmp = tmp.getSibling();
					tmp.setSibling(new Node<E>(child,null,null));
				}
				return true;
			}else{
			return insertChildWrapper(tmp.getSibling(),child,father) || insertChildWrapper(tmp.getChild(),child,father);
			}
		}
		return false;
	}

	/**
	  * A rough representation of the tree is shown on the screen.
      */		
	public void print(){
		Node<E> tmp = root;
		print(tmp);
	}

	/**
      * @param tmp: Temporary node to move on the tree.
      */
	private void print(Node<E> tmp){
		if(tmp != null){
			System.out.print(tmp.getElem()+" ");
			print(tmp.getSibling());				
			System.out.print("\n");
			print(tmp.getChild());
		}
	}

	/**
      * @param elem: element to be cheked.
	  * @return true if the element is contained into the tree. 
      */
	public boolean contains(E elem){
		Node<E> tmp = root;
		return containsWrapper(tmp,elem);
	}

	private boolean containsWrapper(Node<E> tmp,E elem){
		if(tmp != null){
			if(tmp.getElem() == elem)
				return true;
		return containsWrapper(tmp.getSibling(),elem) || containsWrapper(tmp.getChild(),elem);
		}
		return false;
	}	
}