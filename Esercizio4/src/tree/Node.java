package tree;

public class Node<E> {
    private E elem;
    private Node<E> child;
    private Node<E> sibling;

    public Node (E elem, Node<E> child,Node<E> sibling){
			this.elem = elem;
			this.child = child;
			this.sibling = sibling;
    }

    public E getElem(){
			return elem;
    }
	
	public void setElem(E elem){
			this.elem = elem;
    }

    public Node<E> getChild(){
			return child;
    }
    
    public void setChild(Node<E> child){
			this.child = child;
    }
    
    public Node<E> getSibling(){
			return sibling;
    }
    
    public void setSibling(Node<E> sibling){
			this.sibling = sibling;
    }
    
}
