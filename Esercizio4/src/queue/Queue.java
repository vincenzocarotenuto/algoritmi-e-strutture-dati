package queue; 

/**
 *
 * @author Vincenzo Carotenuto
 * @param <E>: type of queue elements
 */

public class Queue<E>{
	private Node<E> front;
	private Node<E> rear;
	
	public Queue(){
		this.front = null;
		this.rear = null;
	}
	
	public Node<E> cons(E elem,Node<E> next){
		Node<E> n = new Node<E>(elem,next);
		return n;
	}
	
	public Node<E> tail(Node<E> n){
		return n.getNext();
	}
	
	public E head(Node<E> n){
		return n.getElem();
	}
	
	public boolean empty(){
		return front == null && rear == null;
	}
	
	public void enqueue(E elem){
		if(empty()){
			front = rear = cons(elem,null);
		}else{
			rear.setNext(cons(elem,null));
			rear = tail(rear);
		}
	}
	
	public E dequeue() throws Exception{
		E elem;
		if(empty()){
			throw new Exception("You can't extract an element if the queue is empty!");
		}else{
			elem = head(front);
			front = tail(front);
			if(front == null)
				rear = null;
		}
		return elem;
	}
	
	public E first(){
		return front.getElem();	
	}
}
