package queue;

public class Node<E> {
    private E elem;
    private Node<E> next;

    public Node (E elem, Node<E> next){
			this.elem = elem;
			this.next = next;
    }

    public E getElem(){
			return elem;
    }

    public Node<E> getNext(){
			return next;
    }
    
    public void setNext(Node<E> node){
			next = node;
    }
    
    public void setElem(E elem){
			this.elem = elem;
    }
}
