/*
Posizionarsi nella cartella src e compilare come segue:

javac -d ../classes sortingarrayusage/*.java

Posizionarsi nella cartella classes ed eseguire il programma

	Per un ordinamento tramite l'algoritmo merge sort come segue

		java -Xmx1g sortingarrayexample/SortingArrayUsageExample -m -a "../integers.csv"

		java -Xmx1g sortingarrayexample/SortingArrayUsageExample -m -d "../integers.csv"

	Per un ordinamento tramite l'algoritmo insertion sort come segue

		java -Xmx1g sortingarrayexample/SortingArrayUsageExample -i -a "../integers.csv"

		java -Xmx1g sortingarrayexample/SortingArrayUsageExample -i -d "../integers.csv"
*/

package sortingarrayexample;
import java.util.Scanner;
import sortingarray.SortingArray;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;

/**
 *
 * Usage:
 *    SortingArrayUsageExample [-m]/[-i] [-a]/[-d] <filename>
 *
 * Options:
 *   -m: merge-sort
 *   -i: insertion-sort
 *	 -a: sorting of items in ascending order
 *	 -d: sorting of items in descending order
 * @author Vincenzo Carotenuto
 */
public class SortingArrayUsageExample {
	static String filename = null;

	public static void main(String[] args) {
		try {
			if(args.length < 3) {
				throw new BadProgramArgumentException("Wrong number of arguments provided to the program.");
			}
			long inizio, fine, time;
			SortingArray<Long> array = new SortingArray<Long>();

			readArray(args[2], array);
			
			inizio = System.currentTimeMillis();
			
			//array.mergeSort(1);
			
			array = sortingMethod(array,args[0],args[1]);

			fine = System.currentTimeMillis();
			time=(fine-inizio)/1000;

			System.out.println("The algorithm took " + time + " seconds to sort the array");

		} catch( Exception e ) {
			System.out.println(e.getMessage());
			//printUsage(e.getMessage());
		}
	}

  /**
   * Returns a SortingArray<Record> based on the given sortingOption.
   *
   * @param  sortingOption a string containing either "-m" or "-i"
   * @return the ordered SortingArray<Record> using the merge sort or insertion sort algorithm
   *
   * @throws BadProgramArgumentException raised if sortingOption is neither "-m" nor "-i".
   */
	private static SortingArray<Long> sortingMethod(SortingArray<Long> array, String sortingAlg, String sortingOption) throws Exception{

		if(sortingAlg.equals("-m")) {
			if(sortingOption.equals("-a")){
				array.mergeSort(1);
			}
			else if(sortingOption.equals("-d")){
				array.mergeSort(0);
			}
			else throw new BadProgramArgumentException("Wrong sorting option given (valid options are -a and -d (ascending and descending order))");
			return array;
		}
		if(sortingAlg.equals("-i")) {
			if(sortingOption.equals("-a")){
				array.insertionSort();
			}
			else if(sortingOption.equals("-d")){
				array.insertionSortDesc();
			}
			else throw new BadProgramArgumentException("Wrong sorting option given (valid options are -a and -d (ascending and descending order))");
			return array;
		}
		throw new BadProgramArgumentException("Wrong sorting option given (valid options are -i and -s)");
	}

  /**
   * Read the contents of the given file and adds each object contained therein to the given sorting array.
   *
   * @param  filename         the name of the file to be read
   * @param  array            the sorting array to be used to store the contents elements of the file
   * @throws IOException      thrown in case any problem arises in reading the contents of the given file.
   */
	private static void readArray(String filename, SortingArray<Long> array) throws Exception {
		BufferedReader reader = new BufferedReader(new FileReader(filename));
		String line;
		while( (line = reader.readLine()) != null ) {
			array.add(Long.parseLong(line));
		}
	}

  /**
   * print the contents of the array onto the console
   * @param  array              [description]
   */
	private static void printArrayContents (SortingArray<Long> array) throws Exception{
		System.out.printf("\n%40s\t\n\n", "[field1]");

		for( int i = 0; i < array.size(); ++i) {
			long record = array.get(i);
			System.out.printf("%40s\t\n", record);
		}
	}

  /**
   * Print a message to the console explaining how to use the program.
   * @param  errorMsg an optional error message to be displayed before the
   *         usage message. If null the message will not be displayed.
   */
	private static void printUsage(String errorMsg) {
		if(errorMsg!=null) {
			System.out.printf("\nERROR: %s\n", errorMsg);

			System.out.printf("\nUsage:\n");
			System.out.printf("\tsorting_array [-m] [-i] filename\n\n");
			System.out.printf("Reads the given file, sorts it, and print it on the console.\n\n");
		}
	}

 /**
   * Exception type used to signal a bad argument given to the program
   */
	private static class BadProgramArgumentException extends Exception {
		static final long serialVersionUID = 0L;

		BadProgramArgumentException(String msg) {
			super(msg);
		}
	}
}
