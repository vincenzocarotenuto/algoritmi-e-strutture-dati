/*
Posizionarsi nella cartella src e compilare come segue:

javac -d ../classes sortingarrayusage/*.java

Posizionarsi nella cartella classes ed eseguire il programma

	Per un ordinamento tramite l'algoritmo merge sort come segue

		java -Xmx1g sortingarrayexample/SortingArrayUsageExample2 "../integers.csv" "../sums.txt"
*/

package sortingarrayexample;
import java.util.Scanner;
import sortingarray.SortingArray;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
/**
 * @author Vincenzo Carotenuto
 */
public class SortingArrayUsageExample2 {

	static String filename = null;

	public static void main(String[] args) {
		try {
			if(args.length < 2) {
				throw new BadProgramArgumentException("Wrong number of arguments provided to the program.");
			}
			long inizio, fine, time;
			SortingArray<Long> array = new SortingArray<Long>();
			
			filename = args[0];
			
			readArray(array);

			inizio = System.currentTimeMillis();

			array.mergeSort(1);
			if(isOnTheArray(array, args[1]) == 0){
				System.out.println("Non esistono due elementi in " + args[0] + " la cui somma \u00E8 presente nel file " + args[1]);
			}
			else{
				System.out.println("Esistono due elementi in " + args[0] + " la cui somma 	\u00E8 presente nel file " + args[1]);
			}
			fine = System.currentTimeMillis();
			time = (fine-inizio)/1000;

			//printArrayContents(array);

			System.out.println("The algorithm took " + time + " seconds");

		} catch( Exception e ) {
			printUsage(e.getMessage());
		}
	}

 /**
  * Read the contents of the given file and adds each object contained therein to the given sorting array.
  *
  * @param  array            the sorting array to be used to store the contents elements of the file
  * @throws IOException      thrown in case any problem arises in reading the contents of the given file.
  */
	private static void readArray(SortingArray<Long> array) throws Exception {
		BufferedReader reader = new BufferedReader(new FileReader(filename));
		String line;
		while( (line = reader.readLine()) != null ) {
			array.add(Long.parseLong(line));
		}
	}

	private static int isOnTheArray(SortingArray<Long> array, String filename) throws Exception{
		BufferedReader reader = new BufferedReader(new FileReader(filename));
		String line;
		long sum, add1, add2;
		int position = 0, flag;

		while( (line = reader.readLine()) != null ) {
			flag =1;
			sum = Long.parseLong(line);
			for (int i = 0; i < array.size()-1 && flag==1; i++){
				add1 = array.get(i);
				add2 = sum - add1;
				position = array.binarySearchRec(0, array.size()-1, add2);
				if(position != -1){
					System.out.println(sum +" = "+add1 +" + "+add2);
					flag = 0;
				}
			}
			if(flag == 0){
				return 1;
			}
		}
		return 0;
	}

 /**
  * print the contents of the array onto the console
  * @param  array              [description]
  */
	private static void printArrayContents (SortingArray<Long> array) throws Exception{
		System.out.printf("\n%40s\t\n\n", "[field1]");

		for( int i = 0; i < array.size(); ++i) {
			long record = array.get(i);
			System.out.printf("%40s\t\n", record);
		}
	}

 /**
  * Print a message to the console explaining how to use the program.
  * @param  errorMsg an optional error message to be displayed before the
  *         usage message. If null the message will not be displayed.
  */
	private static void printUsage(String errorMsg) {
		if(errorMsg!=null) {
		System.out.printf("\nERROR: %s\n", errorMsg);

		System.out.printf("\nUsage:\n");
		System.out.printf("\tsorting_array [-m] [-i] filename\n\n");
		System.out.printf("Reads the given file, sorts it, and print it on the console.\n\n");
		}
	}

/**
  * Exception type used to signal a bad argument given to the program
  */
	private static class BadProgramArgumentException extends Exception {
		static final long serialVersionUID = 0L;

		BadProgramArgumentException(String msg) {
			super(msg);
		}
	}

}
