package sortingarray;

/**
 * @author Vincenzo Carotenuto
 */
 
public class SortingArrayException extends Exception{
	public SortingArrayException(){
		super();
	}
	public SortingArrayException(String messaggio_di_errore){
		super(messaggio_di_errore);
	}
}
