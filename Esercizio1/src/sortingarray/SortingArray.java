package sortingarray;

import java.util.*;

public class SortingArray<E extends Comparable<E>>  {
	
	private ArrayList<E> array;
	
	public SortingArray(){
		array=new ArrayList<>();
	}
	
	public void add(E element) throws SortingArrayException{
		if(element == null){
			throw new SortingArrayException("addElement: element can't be null");
		}
		(this.array).add(size(), element);
	}
	
	public boolean isEmpty(){
		return (this.array).isEmpty();
	}

	public int size(){
		return (this.array).size();
	}
	
	public E get(int i) throws SortingArrayException{
		if((i < 0) || (i >= this.array.size())){
			throw new SortingArrayException("Indice "+ i +" non è valido. L'indice d'essere positivo e inferiore a " + this.array.size());
		}
		return (this.array).get(i);
	}
	
	void swap(int element1, int element2) {
		E obj = array.get(element1);
		array.set( element1, array.get(element2) );
		array.set( element2, obj);
	}
	
	public int compareTo(E value1, E value2) {
		return value1.compareTo(value2);
	}
	
	
	public void insertionSort(){
		int j;
		for (int i=1;i<this.array.size();i++){
			j = i;
			while((j > 0)&&(this.compareTo(array.get(j),array.get(j-1)) <= 0)){
				swap(j, j-1);
				j--;
			}
		}
	}

	public void insertionSortDesc(){
		int j;
		for (int i=1;i<this.array.size();i++){
			j = i;
			while((j > 0)&&(this.compareTo(array.get(j),array.get(j-1))  >= 0)){
				swap(j, j-1);
				j--;
			}
		}
	}
	
/**
  * MERGE
  * 
  */
	protected void merge(ArrayList<E> temp, int left, int mid, int right) {

		int left_end = mid - 1;
		int n = (right - left) + 1;
		int ix = left;

		while (left <= left_end && mid <= right) {
			if (this.compareTo(array.get(left),array.get(mid)) <= 0) {
				temp.set(ix, array.get(left));
				ix++;
				left++;
			} else {
				temp.set(ix, array.get(mid));
				ix++;
				mid++;
			}
		}

		while (left <= left_end) {
			temp.set(ix, array.get(left));
			ix++;
			left++;
		}
		while (mid <= right) {
			temp.set(ix, array.get(mid));
			ix++;
			mid++;
		}

		for (int i = 0; i < n; i++) {
			array.set(right, temp.get(right));
			right--;
		}
	}
	
	protected void mergeDesc(ArrayList<E> temp, int left, int mid, int right) {
		int left_end = mid - 1;
		int n = (right - left) + 1;
		int ix = left;
		while (left <= left_end && mid <= right) {
			if ((this.compareTo(array.get(left),array.get(mid))) >= 0) {
				temp.set(ix, array.get(left));
				ix++;
				left++;
			} else {
				temp.set(ix, array.get(mid));
				ix++;
				mid++;
			}
		}

		while (left <= left_end) {
			temp.set(ix, array.get(left));
			ix++;
			left++;
		}
		while (mid <= right) {
			temp.set(ix, array.get(mid));
			ix++;
			mid++;
		}

		for (int i = 0; i < n; i++) {
			array.set(right, temp.get(right));
			right--;
		}
	}

/**
  *	MERGE SORT
  */
	protected void m_sort(ArrayList<E> temp, int left, int right, int sortingOption){
		if (right > left) {
			int mid = (right + left) / 2; // mid-point
			m_sort(temp, left, mid, sortingOption);
			m_sort(temp, mid + 1, right, sortingOption);
			if (sortingOption == 1)
				merge(temp, left, mid + 1, right);
			else
				mergeDesc(temp, left, mid + 1, right);
		}
	}

/**
  * MERGE SORT INITIALIZATION
  * 
  */
	public void mergeSort(int sortingOption) {
		if (this.array != null && this.array.size() > 1) {
			int len = this.array.size();
			ArrayList<E> temp = new ArrayList<E>(len);
			for (int i = 0; i < len; i++) {
				temp.add(null);
			}
			m_sort(temp, 0, len - 1, sortingOption);
		}
	}
	
	public int binarySearchRec (int low, int high, E key) {
 		if (low > high) return -1;
		int middle;
		middle = (low + high)/2;
		if (this.compareTo(key, array.get(middle)) < 0)
			return binarySearchRec(low, middle-1, key);
		else if (this.compareTo(key, array.get(middle)) > 0)
			return binarySearchRec(middle+1, high, key);
		else 
			return middle;
	}
	
	public void stampa(){
		for (int i=0;i<this.array.size();i++){
			System.out.print(this.array.get(i)+" - ");
		}
	}
}