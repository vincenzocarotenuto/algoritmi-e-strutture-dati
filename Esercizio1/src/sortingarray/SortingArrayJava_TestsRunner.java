package sortingarray;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

/**
 *
 * @author Vincenzo Carotenuto
 */
public class SortingArrayJava_TestsRunner {

/**
 * @param gli argomenti della riga di comando
 */
	public static void main(String[] args) {
		Result result = JUnitCore.runClasses(SortingArrayTests.class);
		for (Failure failure : result.getFailures()) {
			System.out.println(failure.toString());
		}
		System.out.println(result.wasSuccessful());
		
	}
  
}
