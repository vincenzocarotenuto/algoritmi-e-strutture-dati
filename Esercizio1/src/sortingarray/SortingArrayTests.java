/*
Posizionarsi nella cartella src e compilare come segue:

javac -d ../classes sortingarray/SortingArray.java

javac -d ../classes -cp .;C:\ProgramData\JUnit\junit-4.12.jar;C:\ProgramData\JUnit\hamcrest-core-1.3.jar sortingarray/*.java


Posizionarsi nella cartella classes ed eseguire come segue:

java -cp .;C:\ProgramData\JUnit\junit-4.12.jar;C:\ProgramData\JUnit\hamcrest-core-1.3.jar sortingarray.SortingArrayJava_TestsRunner

oppure

java -cp .;C:\ProgramData\JUnit\junit-4.12.jar;C:\ProgramData\JUnit\hamcrest-core-1.3.jar org.junit.runner.JUnitCore sortingarray.SortingArrayTests
*/

package sortingarray;

import java.util.Comparator;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

/**
  *
  *  @author Vincenzo Carotenuto
  */
public class SortingArrayTests {

	


	private Integer element1,element2,element3;
	private SortingArray<Integer> sortingArray;

	@Before
	public void createSortingArray(){
		element1 = 0;
		element2 = 1;
		element3 = 2;
		sortingArray = new SortingArray<>();
	}
	/**
	  * Verify if the array is empty
	  */
	@Test
	public void testIsEmpty(){
		assertTrue(sortingArray.isEmpty());
	}
	/**
	  * Verify if the array is empty
	  */
	@Test
	public void testAdd_oneElement() throws Exception{
		sortingArray.add(element1);
		assertFalse(sortingArray.isEmpty());
	}
	/**
  	  * Verify if the array is empty
	  */
	@Test
	public void testAdd_twoElement() throws Exception{
		sortingArray.add(element1);
		sortingArray.add(element2);
		assertFalse(sortingArray.isEmpty());
	}
	/**
  	  * Verify that the items are arranged in the order in which they were placed
	  */
	@Test
	public void testArray_threeEl() throws Exception{

		Integer[] arrExpected = {element1,element2,element3};

		sortingArray.add(element1);
		sortingArray.add(element2);
		sortingArray.add(element3);

		Integer[] arrActual = new Integer[3];

		for(int i=0;i<3;i++){
			arrActual[i] = sortingArray.get(i);
		}

		assertArrayEquals(arrExpected,arrActual);
	}
	/**
	  * Verify if the method Insertion Sort had sorts the array
	  */
	@Test
	public void testArray_insertionSort() throws Exception{

		Integer[] arrExpected = {element1,element2,element3};

		sortingArray.add(element3);
		sortingArray.add(element2);
		sortingArray.add(element1);

		Integer[] arrActual = new Integer[3];

		sortingArray.insertionSort();

		for(int i=0;i<3;i++){
			arrActual[i] = sortingArray.get(i);
		}

		assertArrayEquals(arrExpected,arrActual);
	}
    /**
	  * Verify if the method Merge Sort had sorts the array
	  */
	@Test
	public void testArray_mergeSort() throws Exception{

		Integer[] arrExpected = {element1,element2,element3};

		sortingArray.add(element3);
		sortingArray.add(element2);
		sortingArray.add(element1);

		Integer[] arrActual = new Integer[3];

		sortingArray.mergeSort(1);

		for(int i=0;i<3;i++){
			arrActual[i] = sortingArray.get(i);
		}

		assertArrayEquals(arrExpected,arrActual);
	}

}
