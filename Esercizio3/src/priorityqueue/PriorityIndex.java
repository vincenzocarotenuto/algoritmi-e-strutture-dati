package priorityqueue;

/**
 *
 * @author Vincenzo Carotenuto
 * @param <T>: type of the priority.
 */
 
public class PriorityIndex<T extends Comparable<T>>{
	private T priority;
	private int index;
	
	public PriorityIndex(){
		priority = null;
		index = -1;
	}

	public PriorityIndex(T priority, int index){
		this.priority = priority;
		this.index = index;
	}
	
	public T getPriority(){
		return priority;
	}
	
	public void setPriority(T priority){
		this.priority = priority;
	}
	
	public int getIndex(){
		return index;
	}
	
	public void setIndex(int index){
		this.index = index;
	}
}