package priorityqueue;

/*
Posizionarsi nella cartella src e compilare come segue:

javac -d ../classes priorityqueue/PriorityQueue.java

javac -d ../classes -cp .;C:\ProgramData\JUnit\junit-4.12.jar;C:\ProgramData\JUnit\hamcrest-core-1.3.jar priorityqueue/*.java


Posizionarsi nella cartella classes ed eseguire come segue:

java -cp .;C:\ProgramData\JUnit\junit-4.12.jar;C:\ProgramData\JUnit\hamcrest-core-1.3.jar priorityqueue.PriorityQueue_TestRunner

oppure

java -cp .;C:\ProgramData\JUnit\junit-4.12.jar;C:\ProgramData\JUnit\hamcrest-core-1.3.jar org.junit.runner.JUnitCore priorityqueue.PriorityQueueTest
*/


import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

public class PriorityQueueTest {
	private Integer i1,i2,i3;
	private Integer p1,p2,p3;
	private PriorityQueue<Integer,Integer> priorityQueue;
  
	@Before
	public void createPriorityArray(){
		i1 =22;
		p1 = 10;
		i2 = -5;
		p2 = -3;
		i3 = 4;
		p3 = 2;
		priorityQueue = new PriorityQueue<>();
	}
	
	@Test
	public void testIsEmpty_zeroEl(){
		assertTrue(priorityQueue.isEmpty());
	}
  
	@Test
	public void testIsEmpty_oneEl() throws Exception{
		priorityQueue.insert(i1,p1);
		assertFalse(priorityQueue.isEmpty());
	}
	
	@Test
	public void testSize_zeroEl() throws Exception{
		assertEquals(0,priorityQueue.heapSize());
	}
  
	@Test
	public void testSize_oneEl() throws Exception{
		priorityQueue.insert(i1,p1);
		assertEquals(1,priorityQueue.heapSize());
	}
  
	@Test
	public void testSize_twoEl() throws Exception{
		priorityQueue.insert(i1,p1);
		priorityQueue.insert(i2,p2);
		assertEquals(2,priorityQueue.heapSize());
	}
	
	@Test
	public void testinsertExtract_oneEl() throws Exception{
		priorityQueue.insert(i3,p3);
		assertTrue(i3==priorityQueue.extractMinimum() && priorityQueue.isEmpty());
	}
	
	@Test
	public void testThreeInsert() throws Exception{
		int i = 0;
		Integer[] arrExpected = {i2,i1,i3};
		
		priorityQueue.insert(i1,p1);
		priorityQueue.insert(i2,p2);
		priorityQueue.insert(i3,p3);
 		
 		while(i < priorityQueue.heapSize()){
			assertTrue(arrExpected[i] == priorityQueue.getElem(i));
 			i++;
 		}
 	}
	
	@Test
	public void testTwoInsertOneExtract() throws Exception{
		int i = 0;
		Integer[] arrExpected = {i3,i1};
		
		priorityQueue.insert(i2,p2);
		priorityQueue.insert(i3,p3);
		priorityQueue.insert(i1,p1);
		priorityQueue.extractMinimum();
 		
 		while(i < priorityQueue.heapSize()){
			int tmp = priorityQueue.getElem(i);
			assertTrue(arrExpected[i] == priorityQueue.getElem(i));
 			i++;
 		}
 	}
	
	@Test
	public void testExtractAll() throws Exception{
		int i = 0;
		Integer[] arrExpected = {};
		
		priorityQueue.insert(i1, p1);
		priorityQueue.insert(i2, p2);
		priorityQueue.insert(i3, p3);
		
		priorityQueue.extractMinimum();
		priorityQueue.extractMinimum();
		priorityQueue.extractMinimum();
		
		assertTrue(0 == priorityQueue.heapSize());
 	}
	
	public void testUpdateOne() throws Exception{
    int i = 0;
    Integer[] arrExpected = {9,7,14,16};
    
    priorityQueue.insert(16, 16);
    priorityQueue.insert(9, 9);
    priorityQueue.insert(14, 14);
		priorityQueue.insert(7, 7);
    
    priorityQueue.updatePriority(7,13);
    
    while(i < priorityQueue.heapSize()){
    	assertTrue(arrExpected[i] == priorityQueue.getElem(i));
 			i++;
 		}				
 	}
}