package priorityqueue;

/*

Posizionarsi nella cartella src e compilare come segue:

javac -d ../classes priorityqueue/SortingArray.java

javac -d ../classes -cp .;C:\ProgramData\JUnit\junit-4.12.jar;C:\ProgramData\JUnit\hamcrest-core-1.3.jar priorityqueue/*.java


Posizionarsi nella cartella classes ed eseguire come segue:

java -cp .;C:\ProgramData\JUnit\junit-4.12.jar;C:\ProgramData\JUnit\hamcrest-core-1.3.jar priorityqueue.PriorityQueue_TestsRunner

oppure

java -cp .;C:\ProgramData\JUnit\junit-4.12.jar;C:\ProgramData\JUnit\hamcrest-core-1.3.jar org.junit.runner.JUnitCore priorityqueue.PriorityQueueTests

*/

import java.util.*;

public class PriorityQueue<T,K extends Comparable<K>>{
	
	private ArrayList<T> heap = null;
	private HashMap<Integer,PriorityIndex<K>> hashmap = null;
	
	public PriorityQueue(){
		this.heap = new ArrayList<T>();
		this.hashmap = new HashMap<Integer,PriorityIndex<K>>();
	}
	
	public boolean isEmpty(){
		return (this.heap).isEmpty();
	}
	
	public int heapSize(){
		return (this.heap).size();
	}
	
	public int indexOf(T elem){
		return heap.indexOf(elem);
	}
	
	public int parent(int i){
		if(i != 0)
			return (i-1)/2;
		return i;
	}
	
	public int leftChild(int i){
		if(2*i+1 < heapSize()){
			return 2*i+1;
		}
		else{
			return i;
		}
	}
	
	public int rightChild(int i){
		if(2*i+2 < heapSize()){
			return 2*i+2;
		}
		else{
			return i;
		}	
	}
	
	public boolean contains(T elem){
		return hashmap.containsKey(elem.hashCode()); 
	}
	
	public void insert(T element, K priority) throws Exception{
		if(priority == null)
			throw new Exception("insert: priority can't be null");
		if(element == null)
			throw new Exception("insert: element can't be null");
		if(contains(element) == true)
			throw new Exception("insert: only one istance for element is allowed");
		int i = -1;
		heap.add(element);
		i = heapSize() - 1;
		PriorityIndex<K> priorityIndex1, priorityIndex2;
		priorityIndex1 = new PriorityIndex<K>(priority,i);
		priorityIndex2 = hashmap.get(heap.get(parent(i)).hashCode());
		hashmap.put(element.hashCode(),priorityIndex1);
		while (i > 0 && priorityIndex1.getPriority().compareTo(priorityIndex2.getPriority()) < 0){
			exchange(parent(i),i);
			i = parent(i);
			priorityIndex1 = hashmap.get(heap.get(i).hashCode());
			priorityIndex2 = hashmap.get(heap.get(parent(i)).hashCode());
		}
	}
	
	private void exchange(int index1,int index2){
		T tmp = heap.get(index1);
		heap.set(index1,heap.get(index2));
		heap.set(index2,tmp);
		hashmap.get(heap.get(index1).hashCode()).setIndex(index1);
		hashmap.get(heap.get(index2).hashCode()).setIndex(index2);
	}
	
	public T extractMinimum()throws Exception{
		if(heapSize() == 0)
			throw new Exception("extractMinimum: can't extract the minimum if size = 0'");
		T root = heap.get(0);
		hashmap.remove(root.hashCode());
		heap.set(0,heap.get(heapSize()-1));
		heap.remove(heapSize()-1);
		if(heapSize() != 0){
			heapify(0);
		}
		return root; 
	}
	
	private void heapify(int i){
		int min = heap.indexOf(min(heap.get(i),heap.get(leftChild(i)),heap.get(rightChild(i))));
		if(min != i){
			exchange(i,min);
			heapify(min);
		}
	}
	
	private T min(T t1,T t2,T t3){
		PriorityIndex<K> p1,p2,p3,pmin;
		p1 = hashmap.get(t1.hashCode());
		p2 = hashmap.get(t2.hashCode());
		p3 = hashmap.get(t3.hashCode());
		pmin = p1;
		T min = t1;
		if(p2.getPriority().compareTo(pmin.getPriority()) < 0){
			min = t2;
			pmin = p2;
		}
		if(p3.getPriority().compareTo(pmin.getPriority()) < 0){
			min = t3;
			pmin = p3;
		}
		return min;
	}
	
	public T getElem(int i) throws Exception{
		if(heapSize() <= i)
			throw new Exception("getElem: can't get the element if size <= i'");
		return heap.get(i);
	}
	
	public K getPriority(T elem){
		return (hashmap.get(elem.hashCode())).getPriority();
	}
	
	public void updatePriority(T elem, K priority) throws Exception{
		if(priority == null)
			throw new Exception("updatePriority: priority can't be null");
		if(elem == null)
			throw new Exception("updatePriority: element can't be null");
		if(!contains(elem))
			throw new Exception("updatePriority: element not found in heap");
		int index = 0;
		PriorityIndex<K> priorityIndex1, priorityIndex2;
		hashmap.get(elem.hashCode()).setPriority(priority);
		priorityIndex1 = hashmap.get(elem.hashCode());
		if(heapSize() == 1)
			index = 0;
		else
			index = priorityIndex1.getIndex();
		priorityIndex2 = hashmap.get(heap.get(parent(index)).hashCode());
		if(priorityIndex1.getPriority().compareTo(priorityIndex2.getPriority()) < 0){
		while(priorityIndex1.getPriority().compareTo(priorityIndex2.getPriority()) < 0){
			exchange(index,parent(index));
			index = parent(index);
			priorityIndex1 = hashmap.get(heap.get(index).hashCode());
			priorityIndex2 = hashmap.get(heap.get(parent(index)).hashCode());
		}
		}else{
			heapify(index);
		}
	}
	
	public void printAll(){
		System.out.println();
		for(int i = 0; i < heapSize(); i++)
			System.out.println(heap.get(i));
	}
}